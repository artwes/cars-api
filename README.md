# Cześć !

## Wprowadzenie
Prosimy Cię o wykonanie prostych aplikacji:h


* backendowej napisanej w języku PHP 7.4. 
* frontendowej napisanej w przy użyciu ES6

Po otrzymaniu Twojego kodu będziemy chcieli ocenić Twój potencjał, 
żeby w następnym etapie na bazie utworzonego przez Ciebie kodu razem spróbować omówić ewnentualne problemy, 
które napotkałaś/eś podczas tworzenia kodu aplikacji.

## Cel zadania

### Warstwa API
W pliku data.json zawarliśmy ustrukturyzowane dane. Korzystając z tych danych wymagamy aplikacji wystawiającej
API z trzema metodami dostępowymi zgodnymi z REST dla encji "samochód" oraz "marka". 
API powinno zwracać dane w formacie JSON i html.

Założenia :

* nie powinieneś/aś korzystać z bibliotek / frameworków budujących API (Symfony / Laravel / FOSRestBundle / ApiPlatform itp)
* użyj bibliotek pomocniczych jeśli uznasz, że warto
* skorzystaj z paradygmatów programowania obiektowego 
* mile widziane użycie wzorców projektowych
* API wystawia metody dostępowe do **wszystkich marek**, do **wszystkich samochodów** oraz do **pojedynczego samochodu**
    ** kontrakt między warstwą frontendową a backendową stanowią zaproponowane obiekty w katalogu ValueObject
    ** wykorzystaj te obiekty
* napisz chociaż jeden test automatyczny (jednostkowy lub funkcjonalny) kluczowej logiki aplikacji
* dla uproszczenia dane dynamicznie odczytujemy z pliku za każdym żądaniem
* utrzymaj code style zgodny z założeniami standardu PSR-2

### Warstwa frontend
Zbuduj prostą aplikację przeglądarkową, w której w trzech osobnych sekcjach zaprezentujesz:

* marki
* dostępne (na podstawie pola "visibility") samochody danej marki po kliknięciu na wybraną markę 
* szczegóły auta po kliknięciu na wybrane auto

Założenia :
* nie korzystaj z frameworków (angular, react, vue)
* użyj bibliotek pomocniczych jeśli uznacz, że warto
* utrzymaj code style zgodny z standardjs (https://standardjs.com/)

### Dokumentacja
W języku angielskim w pliku README.md opisz jak uruchomic projekt.

## Schemat pracy z repozytorium

Prosimy Cię o utworzenie FORK'a aktualnego repozytorium oraz wysłanie adresu forka na adres: 
**developers.php@edpauto.com**.

Pracuj na gałęzi odchodzącej od mastera.
Zespół chciałby wykonać CODE review projektu, który wykonujesz. 

Prosimy Cię więc o przygotowanie Pull Request'ów kiedy uznasz, że wykonałeś :
* 25% części aplikacji
* 75% części aplikacji

i powiadomienie nas na powyższy email.

Podczas przeglądu kodu zweryfikujemy czy dobrze rozumiesz założenia
 i ewentualnie damy wskazówki co można zrobić lepiej :)
 
####Powodzenia !
