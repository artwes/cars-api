<?php

class Car
{
    private int $id;
    private string $make;
    private string $model;
    private int $price;
    private bool $isPublished;
    private string $powerHp;

    public function __construct(
        int $id,
        string $make,
        string $model,
        int $price,
        bool $isPublished,
        string $powerHp
    ) {
        $this->id = $id;
        $this->make = $make;
        $this->model = $model;
        $this->price = $price;
        $this->isPublished = $isPublished;
        $this->powerHp = $powerHp;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getMake(): string
    {
        return $this->make;
    }

    public function getModel(): string
    {
        return $this->model;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function isPublished(): bool
    {
        return $this->isPublished;
    }

    public function getPowerHp(): string
    {
        return $this->powerHp;
    }
}

